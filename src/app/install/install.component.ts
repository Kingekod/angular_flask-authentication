import { Component } from "@angular/core";
import { AuthenticationService, CommandResponse } from "../authentication.service";
import { Router } from "@angular/router";
import { HttpClient } from '@angular/common/http'

@Component({
  templateUrl: "./install.component.html"
})
export class InstallComponent {
  command: CommandResponse = {
	cmd: ""
};

  constructor(private auth: AuthenticationService, private router: Router, private http: HttpClient) {}
  
  win:string
  get:string

  //install(){
  //  return this.http.get<CommandResponse>('/users/install');
  //return this.get
  //}

  install() {
    this.auth.install(this.command).subscribe(
      (data) => {
        //console.log(data.cmd);//this.router.navigateByUrl("/profile");
		if (!data.cmd){
			this.win = 'Выполнено: ' + this.command.cmd		
		}else{
			this.win = ''
		}
      },
      err => {
        console.error(err);
      }
    )
  }
}
